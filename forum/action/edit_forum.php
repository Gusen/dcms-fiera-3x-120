<?php

$forum = mysql_fetch_object(mysql_query('SELECT * FROM `forum` WHERE `id` = ' . abs(intval($_GET['edit_forum']))));

if (user_access('forum_for_edit') && $forum) {
    if (isset($_POST['edit'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $number = abs(intval($_POST['number']));
        $access = abs(intval($_POST['access']));
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum` WHERE `name` = "'.$name.'" AND `id` != '.$forum->id), 0);
        $forum_n = ($forum->number != $number) ? mysql_fetch_object(mysql_query('SELECT `id`, `number` FROM `forum` WHERE `number` = '.$number.' AND `id` != '.$forum->id)) : false;
        if ($forum_n) {
            mysql_query('UPDATE `forum` SET `number` = '.$number.' WHERE `id` = '.$forum->id);
            mysql_query('UPDATE `forum` SET `number` = '.$forum->number.' WHERE `id` = '.$forum_n->id);
        }
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else
        if (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>
                В поле &laquo;название подфорума&raquo; можно использовать от <?= $set['forum_new_them_name_min_pod'] ?> до <?= $set['forum_new_them_name_max_pod'] ?> символов.
            </div>
            <?
        } elseif ($isset_name != 0) {
            ?>
            <div class = 'err'>Подфорум с таким названием уже существует.</div>
            <?
        } elseif ($number < 0 || $number == NULL || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?
        } elseif (mb_strlen($description) > 100) {
            ?>
            <div class = 'err'>Слишком длинное описание подфорума.</div>
            <?
        } else {
            if ($forum->name != $name) {
                admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" переименован в "'.$name.'".');
            } else {
                admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" был изменён.');
            }
            mysql_query('UPDATE `forum` SET `name` = "'.$name.'", `description` = "'.$description.'", `number` = '.$number.', `access` = '.$access.', `output` = '.$output.' WHERE `id` = '.$forum->id);
            $_SESSION['msg'] = '<div class = "msg">Подфорум успешно изменён.</div>';
            header('Location: '.FORUM);
            exit;
        }
    } elseif (isset($_POST['delete']) && user_access('forum_for_delete')) {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name) ?></a> / Удаление
        </div>
        <form action = '<?= FORUM ?>/?edit_forum=<?= $forum->id ?>' method = 'post' style = 'text-align: center'>
            Вы уверены, что хотите удалить этот подфорум со всем его содержимым?<br />
            <input type = 'submit' name = 'del' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
        </form>
        <?
        include_once '../sys/inc/tfoot.php';
    } elseif (isset($_POST['del']) && user_access('forum_for_delete')) {
        admin_log('Форум', 'Подфорумы', 'Подфорум "'.$forum->name.'" был удалён.');
        $themes = mysql_query('SELECT `id` FROM `forum_themes` WHERE `id_forum` = '.$forum->id);
        while ($theme = mysql_fetch_object($themes)) {
            $files = mysql_query('SELECT `name` FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            while ($file = mysql_fetch_object($files)) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            mysql_query('DELETE FROM `forum_post_rating` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_posts` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_post_files` WHERE `id_theme` = '.$theme->id);
            mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
            $votes = mysql_query('SELECT `id` FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
            while ($vote = mysql_fetch_object($votes)) {
                mysql_query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
            }
            mysql_query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
        }
        mysql_query('DELETE FROM `forum_themes` WHERE `id_forum` = '.$forum->id);
        mysql_query('DELETE FROM `forum_razdels` WHERE `id_forum` = '.$forum->id);
        $n_forums = mysql_query('SELECT `id`, `number` FROM `forum` WHERE `number` > '.$forum->number);
        while ($n_forum = mysql_fetch_object($n_forums)) {
            mysql_query('UPDATE `forum` SET `number` = '.($n_forum->number-1).' WHERE `id` = '.$n_forum->id);
        }
        mysql_query('DELETE FROM `forum` WHERE `id` = '.$forum->id);
        $_SESSION['msg'] = '<div class = "msg">Подфорум со всем его содержимым успешно удалён.</div>';
        header('Location: '.FORUM);
        exit;
    } elseif (isset($_GET['cancel'])) {
        header('Location: '.FORUM);
        exit;
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name) ?></a> / Редактирование
    </div>
    <form action = '<?= FORUM ?>/?edit_forum=<?= $forum->id ?>' method = 'post' class="p_m">
        <b>Название подфорума (<?= $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '<?= $forum->name ?>' /><br /><br />
        <b>Описание подфорума (<?= $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</b><br />
        <textarea name = 'description'><?= $forum->description ?></textarea><br /><br />
        <b>Позиция:</b> <input type = 'text' name = 'number' value = '<?= $forum->number ?>' size = '3' /><br /><br />
        <b>Доступ:</b><br />
        <label><input type = 'radio' name = 'access' value = '0' <?= ($forum->access == 0) ? 'checked = "checked"' : NULL ?> /> Все</label><br />
        <label><input type = 'radio' name = 'access' value = '1' <?= ($forum->access == 1) ? 'checked = "checked"' : NULL ?> /> Только администраторы</label><br />
        <label><input type = 'radio' name = 'access' value = '2' <?= ($forum->access == 2) ? 'checked = "checked"' : NULL ?> /> Администраторы + модераторы</label><br />
        <br /><b>Вывод подфорума:</b><br />
        <label><input type = 'checkbox' name = 'output' value = '1' <?= ($forum->output == 1) ? 'checked = "checked"' : NULL ?> /> Отображать список разделов вместо описания</label><br /><br />
        <input type = 'submit' name = 'edit' value = 'Сохранить' />
        <?= user_access('forum_for_delete') ? "<input type = 'submit' name = 'delete' value = 'Удалить' />" : "<input type = 'submit' name = 'cancel' value = 'Отменить' />" ?>
    </form>
    <div class = 'p_m' style = 'text-align: right'><a href = '<?= FORUM ?>'>Отменить редактирование</a></div>
    <?
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM);
}
exit;

?>