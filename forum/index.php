<?php

define('H', $_SERVER['DOCUMENT_ROOT'].'/');

foreach (array('start', 'compress', 'sess', 'settings', 'db_connect', 'ipua', 'fnc', 'user') as $inc) {
    include_once H."sys/inc/{$inc}.php";
}

if (!preg_match("/\bforum\b/i", my_esc($_SERVER['SCRIPT_NAME']))) {
	unset($_SESSION['sort_'.$user['id']]);
}

query("UPDATE `user` SET `url` = '".my_esc($_SERVER['REQUEST_URI'])."' WHERE `id` = '".$user['id']."' LIMIT 1");


include_once 'settings.php'; // Настройки.

$menu = (isset($_GET['menu'])) ? htmlspecialchars($_GET['menu']) : null;
switch($menu) {
    default:
    $set['title'] = (isset($_GET['create_forum'])) ? 'Форум - создание подфорума' : 'Форум';
    include_once '../sys/inc/thead.php';
    title().aut();

    if (isset($_GET['create_forum'])) {
        include_once 'action/create_forum.php'; // Создание подфорума.
    } elseif (isset($_GET['edit_forum'])) {
        include_once 'action/edit_forum.php'; // Редактирование подфорума.
    } else {
        ?>
        <div class = 'menu_razd' style = 'text-align: left'>
            <a href = '<?= FORUM ?>'>Форум</a>
		   : <a href = '<?= FORUM ?>/search.html'>Поиск по форуму</a>
		
		
        </div>
        <?
    }
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    if (user_access('forum_for_create')) {
        ?>
        <div class = 'p_m' style = 'text-align: left'><a href = '<?= FORUM ?>?create_forum'>Создать подфорум</a></div>
        <?
    }
	
    $k_post = mysql_result(query('SELECT COUNT(*) FROM `forum`'), 0);
    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Подфорумы ещё не созданы.</div>
        <?
    } else {
        ?>
        <table class="post">
            <?
            $forums = query('SELECT * FROM `forum` ORDER BY `number` ASC LIMIT '.$start.', '.$set['p_str']);
            while ($forum = mysql_fetch_object($forums)) {
                if ($forum->access == 0 || ($forum->access == 1 && $user['group_access'] > 7) || ($forum->access == 2 && $user['group_access'] > 2)) {
                    $count_razdels = mysql_result(query('SELECT COUNT(*) FROM `forum_razdels` WHERE `id_forum` = '.$forum->id), 0);
                    $count_themes = mysql_result(query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_forum` = '.$forum->id), 0);
                    ?>
                    <tr>
                        <td class = 'icon14'>
                            <img src = '<?= FORUM ?>/icons/forum.png' alt = '' <?= ICONS ?> />
                        </td>
                        <td class = 'p_t'>
                            <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> (<?= $count_razdels.'/'.$count_themes ?>)
                        </td>
                        <?
                        if (user_access('forum_for_edit')) {
                            ?>
                            <td class = 'icon14'>
                                <a href = '<?= FORUM.'/?edit_forum='.$forum->id ?>'><img src = '<?= FORUM ?>/icons/edit.png' alt = '' <?= ICONS ?> /></a>
                            </td>
                            <?
                        }
                        ?>
                    </tr>
                    <?
                    if ($forum->description != NULL && $forum->output == 0) {
                        ?>
                        <tr>
                            <td class = 'p_m' colspan = '3'>
                                <?= output_text($forum->description, 1, 1, 0, 1, 1) ?>
                            </td>
                        </tr>
                        <?
                    } elseif ($forum->output == 1 && $count_razdels > 0) {
                        ?>
                        <tr>
                            <td class = 'p_m' colspan = '3'>
                                <?
                                $razdels = query('SELECT `id`, `name` FROM `forum_razdels` WHERE `id_forum` = '.$forum->id);
                                while ($razdel = mysql_fetch_object($razdels)) {
                                    ?>
                                    <img src = '<?= FORUM ?>/icons/razdel.png' alt = '' style = 'width: 16px; height: 16px' /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name) ?></a><br />
                                    <?
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                }
            }
            ?>
        </table>
        <?
        if ($k_page > 1) {
            str('?', $k_page, $page);
        }
    }
    $new_themes = mysql_result(query('SELECT COUNT(*) FROM `forum_themes` WHERE `time` > '.(time()-60*60*24)), 0);
    $my_themes = (isset($user)) ? mysql_result(query('SELECT COUNT(*) FROM `forum_themes` WHERE `id_user` = '.$user['id']), 0) : NULL;
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a>
    </div>
    <div class = 'p_t' style = 'text-align: left'>
        <a href = '<?= FORUM ?>/new_themes.html'>Новые темы</a> (<?= $new_themes ?>)
        <?
        if (isset($user)) {
            ?>
             | <a href = '<?= FORUM ?>/my_themes.html'>Мои темы</a> (<?= $my_themes ?>)
            <?
        }
        ?>
    </div>
 
    <?
    break;
    case 'forum':
        include_once 'view/forum.php'; // Подфорум.
    break;
    case 'razdel':
        include_once 'view/razdel.php'; // Раздел.
    break;
    case 'theme':
        include_once 'view/theme.php'; // Тема.
    break;
    case 'who':
        include_once 'view/who.php'; // Кто в теме.
    break;
    case 'files':
        include_once 'view/files_theme.php'; // Файлы темы.
    break;
    case 'add_file':
        include_once 'action/add_file_post.php'; // Добавление файла.
    break;
    case 'reports':
        include_once 'view/reports.php'; // Жалобы на темы.
    break;
    case 'new_themes':
        include_once 'view/new_themes.php'; // Новые темы.
    break;
    case 'my_themes':
        include_once 'view/my_themes.php'; // Новые темы.
    break;
    case 'search':
        include_once 'view/search.php'; // Поиск по форуму.
    break;
}
include_once '../sys/inc/tfoot.php';

?>